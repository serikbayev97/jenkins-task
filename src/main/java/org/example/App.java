package org.example;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        var num = 0;
        int res1 = add(num, 5);
        int res2 = subtract(res1, 1);
        int res3 = multiply(res2, 3);
        int res4 = divide(res3, 2);
        System.out.println("Result: " + res4);


        var array = new int[] {1,2,3};
        var sum = sum(array);
        System.out.println("Sum: " +  sum);
    }



    public static int add(int x,  int y) {
        return x + y;
    }

    public static int subtract(int x, int y) {
        return x - y;
    }

    public static int multiply(int x, int y) {
        return x * y;
    }

    public static int divide(int x, int y) {
        return x / y;
    }

    public static int sum(int[] array) {
        var sum = 0;
        for (int index : array) {
            sum += index;
        }
        return sum;
    }
}
